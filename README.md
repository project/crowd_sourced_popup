CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

This module allows participating sites to display the `crowd-sourced` popup. Its message is displayed once per browser session.

The underlying code and configuration were straight up jacked from [these fine folk](https://www.drupal.org/docs/contributed-modules/simple-popup-blocks).

REQUIREMENTS
------------

No specific requirements.

INSTALLATION
------------

Install normally as other modules are installed. For Support:
https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules

With `composer`:

```
composer require drupal/crowd_sourced_popup:1.0.0
```

With `ddev`/`composer`:

```
ddev composer require drupal/crowd_sourced_popup:1.0.0
```

CONFIGURATION
----------------------
Go to admin/settings/crowd_sourced_popup and set message title and body.
Go to admin/user/permissions and set permissions.

