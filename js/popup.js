/**
 * @file
 * jQuery code.
 * Based on code: Adrian "yEnS" Mato Gondelle, twitter: @adrianmg
 * Modifications for Drupal: Grzegorz Bartman grzegorz.bartman@openbit.pl
 *                           Pawel Gorski gp@gorskipawel.pl
 */
(function ($, Drupal, drupalSettings, cookies) {
  'use strict';

  var popupStatus = 0;

  /**
   * Loading popup with jQuery.
   */
  function crowd_sourced_popup_load_popup(done) {
    // Loads json with modal settings
    $.getJSON('/crowd_sourced_popup/status?popup_path=' + window.location.pathname, function (json) {
      if (json.status === 1) {
        popupStatus = 1;

        // Set cookie
        var timestamp = (+new Date());

        var ttl = parseInt(drupalSettings.popupMessage.expire);
        if (ttl > 0) {
          cookies.set('crowd_sourced_popup_displayed', timestamp, {path: '/', expires: ttl});
        }
        else {
          cookies.set('crowd_sourced_popup_displayed', timestamp, {path: '/'});
        }
      }

      done(popupStatus);
    });
  }

  /**
   * Disabling popup with jQuery.
   */
  function crowd_sourced_popup_disable_popup() {
    // Disables popup only if it is enabled.
    if (popupStatus === 1) {
      $('#popup-message-background').fadeOut('slow');
      $('#popup-message-wrapper').fadeOut('slow');
      $('#popup-message-content').empty().remove();
      popupStatus = 0;
    }
  }

  /**
   * Centering popup
   *
   * @param {number} width
   *   Width css attribute.
   * @param {number} height
   *   Height css attribute.
   */
  function crowd_sourced_popup_center_popup(width, height) {
    // Request data for centering.
    var windowHeight = document.documentElement.clientHeight;

    var popupWidth = 0;
    if (typeof width === 'undefined') {
      popupWidth = $('#popup-message-window').width();
    }
    else {
      popupWidth = width;
    }
    var popupHeight = 0;
    if (typeof width === 'undefined') {
      popupHeight = $('#popup-message-window').height();
    }
    else {
      popupHeight = height;
    }

    // Centering.
    $('#popup-message-window').css({
      position: 'absolute',
      width: popupWidth + 'px',
      height: popupHeight + 'px'
    });

    // Only need force for IE6.
    $('#popup-message-background').css({
      height: windowHeight
    });
  }

  /**
   * Display popup message.
   *
   * @param {string} crowd_sourced_popup_title
   *   Message title.
   * @param {string} crowd_sourced_popup_body
   *   Message body.
   * @param {number} width
   *   Message box width.
   * @param {number} height
   *   Message box height.
   */
  function crowd_sourced_popup_display_popup(crowd_sourced_popup_source_url) {
    crowd_sourced_popup_load_popup(showPopup => {
      if (showPopup) {
        $('head').append('<link rel="stylesheet" href="' + crowd_sourced_popup_source_url + '/embed/css/styles.css">');

        $('body').append('<script type="text/javascript" src="' + crowd_sourced_popup_source_url + '/embed/app.js" async="true" defer="true"></script>');
      }
    });
  }

  /**
   * Helper function for get last element from object.
   * Used if on page is loaded more than one message.
   *
   * @param {Object} variable_data
   *   Check_cookie data.
   *
   * @return {*}
   *   Last object item.
   */
  function crowd_sourced_popup_get_last_object_item(variable_data) {
    if (typeof (variable_data) === 'object') {
      variable_data = variable_data[(variable_data.length - 1)];
    }
    return variable_data;
  }

  Drupal.behaviors.popupMessage = {
    attach: function (context) {

      let cswrapper = $('#cswrapper').get();
      if (!cswrapper.length) {
        $('body').append('<div id="cswrapper"></div>');
      }


      once('wrapped', '#cswrapper').forEach(element => {

        var timestamp = (+new Date());
        var check_cookie = drupalSettings.popupMessage.check_cookie;
        check_cookie = crowd_sourced_popup_get_last_object_item(check_cookie);
        var crowd_sourced_popup_cookie = cookies.get('crowd_sourced_popup_displayed');
        var delay = drupalSettings.popupMessage.delay * 1000;
        var close_delay = drupalSettings.popupMessage.close_delay * 1000;
        var show_popup = false;
        if (!crowd_sourced_popup_cookie || check_cookie === '0') {
          show_popup = true;
        }

        if (show_popup) {
          var run_popup = function () {
            // Get variables.
            var crowd_sourced_popup_source_url = drupalSettings.popupMessage.url;
            crowd_sourced_popup_source_url = crowd_sourced_popup_get_last_object_item(crowd_sourced_popup_source_url);
            crowd_sourced_popup_display_popup(crowd_sourced_popup_source_url);
          };

          var trigger_time = delay;
          setTimeout(run_popup, trigger_time);

          // Auto close
          if (close_delay > 0) {
            var close_popup = function () {
              crowd_sourced_popup_disable_popup();
            };

            var close_trigger_time = trigger_time + close_delay;
            setTimeout(close_popup, close_trigger_time);
          }
        }
      });
    }
  };
})(jQuery, Drupal, drupalSettings, window.Cookies, once);
