<?php

namespace Drupal\crowd_sourced_popup\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Asset\AssetCollectionGroupOptimizerInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide form for settings crowd_sourced_popup module.
 *
 * @package Drupal\crowd_sourced_popup\Form
 */
class PopupMessageSettingsForm extends ConfigFormBase {

  const POPUP_MESSAGE_CSS_NAME = 'popup.css';

  /**
   * CssCollectionOptimizer service.
   *
   * @var \Drupal\Core\Asset\CssCollectionOptimizerLazy
   */
  protected AssetCollectionGroupOptimizerInterface $cssOptimizer;

  /**
   * JsCollectionOptimizer service.
   *
   * @var \Drupal\Core\Asset\JsCollectionOptimizerLazy
   */
  protected AssetCollectionGroupOptimizerInterface $jsOptimizer;

  /**
   * Drupal\Core\Entity\EntityRepositoryInterface service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * The cache tags invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected CacheTagsInvalidatorInterface $cacheTagsInvalidator;

  /**
   * Drupal\Core\Extension\ModuleExtensionList service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  private ModuleExtensionList $moduleExtensionList;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);
    $instance->cssOptimizer = $container->get('asset.css.collection_optimizer');
    $instance->jsOptimizer = $container->get('asset.js.collection_optimizer');
    $instance->entityRepository = $container->get('entity.repository');
    $instance->moduleExtensionList = $container->get('extension.list.module');
    $instance->cacheTagsInvalidator = $container->get('cache_tags.invalidator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'crowd_sourced_popup_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('crowd_sourced_popup.settings');

    $form['crowd_sourced_popup_enable'] = [
      '#type' => 'radios',
      '#title' => $this->t('Enable Popup'),
      '#default_value' => $config->get('enable') ? $config->get('enable') : 0,
      '#options' => [
        1 => $this->t('Enabled'),
        0 => $this->t('Disabled'),
      ],
    ];

    $form['crowd_sourced_popup_fieldset']['cookie_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cookie settings'),
    ];

    $form['crowd_sourced_popup_fieldset']['cookie_settings']['crowd_sourced_popup_check_cookie'] = [
      '#type' => 'radios',
      '#title' => $this->t('Check cookie'),
      '#description' => $this->t('If enabled message will be displayed only once per browser session'),
      '#default_value' => $config->get('check_cookie') ? $config->get('check_cookie') : 0,
      '#options' => [
        1 => $this->t('Enabled'),
        0 => $this->t('Disabled'),
      ],
    ];

    $form['crowd_sourced_popup_fieldset']['cookie_settings']['crowd_sourced_popup_expire'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie lifetime in days'),
      '#description' => $this->t('Define lifetime of the cookie in days. Message will not reappear until the expiration time is exceeded. If 0, popup will reappear if browser has been closed.'),
      '#default_value' => $config->get('expire') ? $config->get('expire') : 0,
    ];

    // Styles.
    // Find styles in module directory.
    $directory = $this->moduleExtensionList->getPath('crowd_sourced_popup') . '/styles';
    $subdirectories = scandir($directory);
    $styles = [];

    foreach ($subdirectories as $subdirectory) {
      if (is_dir($directory . '/' . $subdirectory)) {
        if (file_exists($directory . '/' . $subdirectory . '/' . self::POPUP_MESSAGE_CSS_NAME)) {
          $lib_path = $subdirectory . '/' . self::POPUP_MESSAGE_CSS_NAME;
          $styles[$lib_path] = $subdirectory;
        }
      }
    }

    $form['crowd_sourced_popup_fieldset']['visibility']['path'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Pages'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#group' => 'visibility',
      '#weight' => 0,
    ];

    $options = [
      $this->t('All pages except those listed'),
      $this->t('Only the listed pages'),
    ];

    $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.",
      [
        '%blog' => 'blog',
        '%blog-wildcard' => 'blog/*',
        '%front' => '<front>',
      ]
    );

    $title = $this->t('Pages');

    $form['crowd_sourced_popup_fieldset']['visibility']['path']['crowd_sourced_popup_visibility'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show block on specific pages'),
      '#options' => $options,
      '#default_value' => $config->get('visibility') ? $config->get('visibility') : 0,
    ];

    $form['crowd_sourced_popup_fieldset']['visibility']['path']['crowd_sourced_popup_visibility_pages'] = [
      '#type' => 'textarea',
      '#default_value' => $config->get('visibility_pages') ? $config->get('visibility_pages') : '',
      '#description' => $description,
      '#title' => '<span class="element-invisible">' . $title . '</span>',
    ];

    // Popup software endpoint
    $form['crowd_sourced_popup_fieldset']['source_url'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Popup source'),
    ];

    $form['crowd_sourced_popup_fieldset']['source_url']['crowd_sourced_popup_url'] = [
      '#type' => 'url',
      '#default_value' => $config->get('url') ? $config->get('url') : '',
      '#description' => 'The HTTPS endpoint from which to obtain the crowd-sourced software',
      '#title' => '<span class="element-invisible">URL</span>',
    ];



    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $save = FALSE;
    $flush_cache_css = FALSE;
    $flush_cache_js = FALSE;

    $keys = [
      'check_cookie',
      'expire',
      'url',
      'visibility',
      'visibility_pages',
    ];

    $config = $this->config('crowd_sourced_popup.settings');
    foreach ($keys as $key) {
      $value = $form_state->getValue('crowd_sourced_popup_' . $key);
      if ($config->get($key) != $value) {
        $save = TRUE;
        $config->set($key, $value);

        if ($key === 'style') {
          $flush_cache_css = TRUE;
        }
        elseif ($key === 'check_cookie') {
          $flush_cache_js = TRUE;
        }
      }
    }

    //$text = $form_state->getValue('crowd_sourced_popup_body')['value'];
    $text = 'test';
    $uuids = $this->extractFilesUuid($text);
    $this->recordFileUsage($uuids);

    if ($save) {
      $config->save();
      $this->cacheTagsInvalidator->invalidateTags(['rendered']);
    }

    if ($flush_cache_css) {
      $this->cssOptimizer->deleteAll();
      $this->cacheTagsInvalidator->invalidateTags(['library_info']);
    }
    if ($flush_cache_js) {
      $this->jsOptimizer->deleteAll();
      $this->cacheTagsInvalidator->invalidateTags(['library_info']);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'crowd_sourced_popup.settings',
    ];
  }

  /**
   * Parse an HTML snippet for any linked file with data-entity-uuid attributes.
   *
   * @param string $text
   *   The partial (X)HTML snippet to load. Invalid markup will be corrected on
   *   import.
   *
   * @return array
   *   An array of all found UUIDs.
   */
  protected function extractFilesUuid(string $text): array {
    $dom = Html::load($text);
    $xpath = new \DOMXPath($dom);
    $uuids = [];
    foreach ($xpath->query('//*[@data-entity-type="file" and @data-entity-uuid]') as $file) {
      $uuids[] = $file->getAttribute('data-entity-uuid');
    }

    return $uuids;
  }

  /**
   * Records file usage of files referenced by formatted text fields.
   *
   * Every referenced file that does not yet have the FILE_STATUS_PERMANENT
   * state, will be given that state.
   *
   * @param array $uuids
   *   An array of file entity UUIDs.
   */
  protected function recordFileUsage(array $uuids): void {
    try {
      foreach ($uuids as $uuid) {
        if ($file = $this->entityRepository->loadEntityByUuid('file', $uuid)) {
          /** @var \Drupal\file\FileInterface $file */
          if (!$file->isPermanent()) {
            $file->setPermanent();
            $file->save();
          }
        }
      }
    }
    catch (EntityStorageException $exception) {
      $this->logger('crowd_sourced_popup')->warning($exception->getMessage());
    }
  }

}
