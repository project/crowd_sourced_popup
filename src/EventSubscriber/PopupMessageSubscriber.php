<?php

namespace Drupal\crowd_sourced_popup\EventSubscriber;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Path\PathMatcher;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Provide subscriber event for crowd_sourced_popup.
 *
 * @package Drupal\crowd_sourced_popup\EventSubscriber
 */
class PopupMessageSubscriber implements EventSubscriberInterface {

  /**
   * The PopupMessage config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Current Request.
   *
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $requestStack;

  /**
   * Path matcher services.
   *
   * @var \Drupal\Core\Path\PathMatcher
   */
  protected $pathMatcher;

  /**
   * User account service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * PopupMessageSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Popup_message config.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Current Request.
   * @param \Drupal\Core\Path\PathMatcher $pathMatcher
   *   Path matcher services.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler service.
   */
  public function __construct(ConfigFactoryInterface $config, RequestStack $requestStack, PathMatcher $pathMatcher, AccountInterface $account, ModuleHandlerInterface $moduleHandler) {
    $this->config = $config->get('crowd_sourced_popup.settings');
    $this->requestStack = $requestStack->getCurrentRequest();
    $this->pathMatcher = $pathMatcher;
    $this->account = $account;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Init PopupMessage.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   PopupMessage event.
   */
  public function showPopupMessage(ResponseEvent $event) {
    // Check permissions to display message.
    $response = $event->getResponse();

    if (!$response instanceof AttachmentsInterface) {
      return;
    }

    // Get the drupal language setting
    $lang_id = \Drupal::languageManager()->getCurrentLanguage()->getId();

    // Check module has enable popup.
    $status = $this->config->get('enable');

    // Omit system path.
    $current_url = $this->requestStack->getRequestUri();
    $decline_system_path = ('/editor/*');
    $system_path = $this->pathMatcher->matchPath($current_url, $decline_system_path);

    // Check module has enable popup, permission, exclude denied url.
    // Set session with true or false.
    // If all requirements are ok session PopupMessageStatus is set to true.
    if ($status && !$system_path) {
      $permission = $this->account->hasPermission('display crowd sourced popup');

      // Get status: enabled/disabled.
      // Allow other modules to modify permissions.
      $this->moduleHandler->alter('crowd_sourced_popup_permission', $permission);

      $crowd_sourced_popup_parameters = [
        'check_cookie' => $this->config->get('check_cookie') ?? 0,
        'expire' => $this->config->get('expire') ?? 0,
        'url' => $this->config->get('url') ?? '',
        'lang' => $lang_id ?? 'en',
      ];

      // Allow other modules to modify message parameters.
      $this->moduleHandler->alter('crowd_sourced_popup_parameters', $crowd_sourced_popup_parameters);

      $attachments = $response->getAttachments();
      $attachments['library'][] = 'crowd_sourced_popup/crowd_sourced_popup_style';
      $attachments['drupalSettings']['popupMessage'] = $crowd_sourced_popup_parameters;
      $response->setAttachments($attachments);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['showPopupMessage', 20];

    return $events;
  }

}
